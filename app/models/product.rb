class Product < ApplicationRecord
  validates :title, :description, :image_url, :price, presence: true
  validates_numericality_of :price, greater_than_or_equal_to: 0.01
  validates_uniqueness_of :title
  validates_format_of :image_url,
                      allow_blank: true,
                      with: %r{\.(gif|jpg|png)\Z}i,
                      message: 'must be a URL for GIT, JPG or PNG image'

  has_many :line_items
  has_many :orders, through: :line_items
  before_destroy :ensure_not_referenced_by_line_items


  private

  def  ensure_not_referenced_by_line_items
    unless line_items.empty?
      errors.add(:base, 'Line Items present')
      throw :abort
    end
  end

end
